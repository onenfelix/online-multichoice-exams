# Generated by Django 2.2 on 2020-08-24 12:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exams', '0019_auto_20200824_1405'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answer',
            name='answer_value',
            field=models.IntegerField(blank=True, default=0, null=True),
        ),
    ]
