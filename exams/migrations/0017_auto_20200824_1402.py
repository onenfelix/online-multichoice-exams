# Generated by Django 2.2 on 2020-08-24 11:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exams', '0016_settings_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='result',
            name='correct_option',
        ),
        migrations.RemoveField(
            model_name='result',
            name='option_1',
        ),
        migrations.RemoveField(
            model_name='result',
            name='option_2',
        ),
        migrations.RemoveField(
            model_name='result',
            name='option_3',
        ),
        migrations.RemoveField(
            model_name='result',
            name='option_4',
        ),
        migrations.RemoveField(
            model_name='result',
            name='option_5',
        ),
        migrations.RemoveField(
            model_name='result',
            name='question_text',
        ),
        migrations.AddField(
            model_name='result',
            name='email',
            field=models.EmailField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='result',
            name='marks',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='result',
            name='percentage',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='result',
            name='username',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
