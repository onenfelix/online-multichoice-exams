# Generated by Django 2.2 on 2020-08-25 08:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exams', '0021_auto_20200825_1042'),
    ]

    operations = [
        migrations.AddField(
            model_name='monitor',
            name='end_time',
            field=models.TimeField(blank=True, null=True),
        ),
    ]
