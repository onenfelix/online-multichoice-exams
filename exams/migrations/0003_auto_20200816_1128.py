# Generated by Django 2.2 on 2020-08-16 08:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exams', '0002_auto_20200815_1614'),
    ]

    operations = [
        migrations.RenameField(
            model_name='result',
            old_name='correct_options',
            new_name='correct_option',
        ),
        migrations.RenameField(
            model_name='result',
            old_name='question_texts',
            new_name='question_text',
        ),
        migrations.RemoveField(
            model_name='result',
            name='answer_texts',
        ),
        migrations.AddField(
            model_name='result',
            name='option_1',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AddField(
            model_name='result',
            name='option_2',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AddField(
            model_name='result',
            name='option_3',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AddField(
            model_name='result',
            name='option_4',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AddField(
            model_name='result',
            name='option_5',
            field=models.CharField(blank=True, max_length=200),
        ),
    ]
