# Generated by Django 2.2 on 2020-08-19 05:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exams', '0006_auto_20200818_1746'),
    ]

    operations = [
        migrations.AddField(
            model_name='result',
            name='correct_option',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AddField(
            model_name='result',
            name='question_text',
            field=models.TextField(blank=True),
        ),
    ]
